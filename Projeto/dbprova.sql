create database dbprova;
use dbprova;

create table abelhas(
nome varchar(255),
taxonomia varchar(255),
primary key (nome)
);

create table flores(
nome varchar(255),
mes varchar(255),
primary key (nome)
);

create table abelhasXflores (
nomeabelha varchar(255),
nomeflor varchar(255),
constraint PK_abelhasXflores primary key (nomeabelha, nomeflor),
constraint FK_abelhasXflores foreign key (nomeabelha)
references abelhas(nome),
constraint FK_abelhasXflores2 foreign key (nomeflor)
references flores(nome)
);

insert into abelhas values ('Urucu', 'Melipona Scutellaris');
insert into abelhas values ('Urucu-Amarela', 'Melipona Rufiventris');
insert into abelhas values ('Guarupu', 'Melipona Bicolor');
insert into abelhas values ('Irai', 'Melipona Testaceicornes');