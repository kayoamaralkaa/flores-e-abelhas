package com.mycompany.prova.backend;

public class Flor {
    private String nome;
    private String mes;
    
    public Flor(){
        
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }
    
    public void setMes(String mes){
        this.mes = mes;
    }
    
    public String getNome(){
        return this.mes;
    }
    
    public String getMes(){
        return mes;
    }
    
    
    
    @Override
    public String toString(){
        return "Flor: " +nome + "/n Floresce: " + mes;
    }

}