package com.mycompany.prova.base;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DataBase {
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost:3306/dbprova";
    private static final String USER = "root";
    private static final String PASS = "admin";
    
    public static Connection getConnection(){
        try {
            Class.forName(DRIVER);
            return DriverManager.getConnection(URL, USER, PASS);
        } catch (ClassNotFoundException | SQLException ex) {
            throw new RuntimeException("Erro ao conectar a base de dados! Erro: ", ex);
        }
    }
    
    public static void closeConnection(Connection connection){
        try {
            if(connection != null){    
                connection.close();
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Falha ao fechar a conexão com a base de dados! Erro: ", ex); 
        }
    }
    
    public static void closeConnection(Connection connection, PreparedStatement statement){
        closeConnection(connection);
        try {
            if(statement != null){    
                statement.close();
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Falha ao fechar a conexão com a base de dados! Erro: ", ex); 
        }
    }
    
    public static void closeConnection(Connection connection, PreparedStatement statement, ResultSet result){
        closeConnection(connection, statement);
        try {
            if(result != null){    
                result.close();
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Falha ao fechar a conexão com a base de dados! Erro: ", ex); 
        }
    }
    
}
