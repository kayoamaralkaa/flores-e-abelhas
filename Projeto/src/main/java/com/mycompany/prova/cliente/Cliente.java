package com.mycompany.prova.cliente;

import com.mycompany.prova.dao.FlorDAO;
import com.mycompany.prova.backend.Flor;

public class Cliente {
    
    public static void main(String[] args) {
        //Instanciando uma flor.
        Flor flor = new Flor();
        //Inserindo o nome da flor.
        flor.setNome("urucum");
        //Inserindo o mês de floração.
        flor.setMes("Janeiro");
        //Instanciando um comando do Banco de Dados.
        FlorDAO florDAO = new FlorDAO();
        //Inserindo uma flor no Banco de Dados.
        florDAO.createFlor(flor);
        //Adicionando uma abelha à uma flor.
        florDAO.createFlorEAbelha(flor.getNome(), "Urucu");
        //Pesquisa de flores por abelha. (Pode ser feita com o campo vazio para exibir todas as flores)
        System.out.println(florDAO.readByAbelha("Urucu"));
        //Pesquisa de flores por Mês. (Pode ser feita com o campo vazio para exibir todas as flores)
        System.out.println(florDAO.readByMes(flor.getMes()));
    }
    
}
