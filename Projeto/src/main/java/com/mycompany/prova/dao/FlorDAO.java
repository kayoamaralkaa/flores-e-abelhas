package com.mycompany.prova.dao;

import com.mycompany.prova.backend.Flor;
import com.mycompany.prova.base.DataBase;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class FlorDAO {
    
    public void createFlor(Flor flor){
        Connection connection = DataBase.getConnection();
        PreparedStatement statement = null;
        
        try{
            statement = connection.prepareStatement("INSERT INTO flores (nome, mes) values (?,?)");
            statement.setString(1, flor.getNome());
            statement.setString(2, flor.getMes());
            statement.executeUpdate();
        } catch (SQLException ex) {
            throw new RuntimeException ("Erro ao sincronizar com a base de dados", ex);
        } finally {
            DataBase.closeConnection(connection, statement);
        }
    }
    
    public void createFlorEAbelha(String nomeFlor, String nomeAbelha){
        Connection connection = DataBase.getConnection();
        PreparedStatement statement = null;
        
        try{
            statement = connection.prepareStatement("INSERT INTO abelhasxflores (nomeabelha, nomeflor) values (?,?)");
            statement.setString(1, nomeAbelha);
            statement.setString(2, nomeFlor);
            statement.executeUpdate();
        } catch (SQLException ex) {
            throw new RuntimeException ("Erro ao sincronizar com a base de dados", ex);
        } finally {
            DataBase.closeConnection(connection, statement);
        }
    }
    
    public ArrayList <String> readByAbelha(String nomeAbelha){
        Connection connection = DataBase.getConnection();
        PreparedStatement statement = null; 
        ResultSet result = null;
        ArrayList <String> flores = new ArrayList();
        
        try {
            if(!nomeAbelha.equals("")){
                statement = connection.prepareStatement("SELECT nomeflor FROM abelhasxflores WHERE nomeabelha = ?");
                statement.setString(1, nomeAbelha);
                result = statement.executeQuery();
                while(result.next()){
                    flores.add(result.getString("nomeflor"));
                }
            } else {
                statement = connection.prepareStatement("SELECT nomeflor FROM abelhasxflores");
                result = statement.executeQuery();
                while(result.next()){
                    flores.add(result.getString("nomeflor"));
                }
            }
        } catch (SQLException ex) {
            throw new RuntimeException ("Erro ao sincronizar com a base de dados", ex);
        }

        return flores;
    }
    
    public ArrayList <String> readByMes(String mes){
        Connection connection = DataBase.getConnection();
        PreparedStatement statement = null; 
        ResultSet result = null;
        ArrayList <String> flores = new ArrayList();
        
        try {
            if(!mes.equals("")){
                statement = connection.prepareStatement("SELECT nomeflor FROM flores WHERE mes = ?");
                result = statement.executeQuery();
                while(result.next()){
                    flores.add(result.getString("nomeflor"));
                }
            } else {
                statement = connection.prepareStatement("SELECT nomeflor FROM abelhasxflores");
                result = statement.executeQuery();
                while(result.next()){
                    flores.add(result.getString("nomeflor"));
                }
            }
        } catch (SQLException ex) {
            throw new RuntimeException ("Erro ao sincronizar com a base de dados", ex);
        }

        return flores;
    }
    
}
