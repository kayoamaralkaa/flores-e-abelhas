# Flores e Abelhas
Conforme foi acordado na entrevista, escolhi fazer o projeto em Java utilizando Orientação a Objetos e suas regras por motivo de afinidade e organização.
Usei um banco MySQL para armazenar os dados da aplicação, meus testes foram limitados pois tive problemas com o plugin JDBC do MySQL.
Neste repositório está contido o código-fonte da aplicação que pode ser excutada como simulação no Prompt de Comando, porém a simulação pode ser alterada no código-fonte, na Classe Cliente.java, onde está comentado o seu passo a passo. Além disso está contido o Banco de Dados num estado antes da inserção de dados da aplicação (que foi prejudicada devido a falha no JDBC).
No Banco de Dados estão as Tabelas de "Abelhas", "Flores" e "AbelhasXFlores": já que foi necessário criar um relacionamento N:N entre as tabelas "Abelhas" e "Flores" devido ao mecanismo de pesquisa exigido.

Me desculpem a simplicidade da aplicação e a demora, gostaria de ter feito as telas e entregar a aplicação em forma de executável, mas como tive imprevistos fiquei devendo. Espero que a estrutura base esteja ao agrado.
